import java.util.Scanner;


public class Play {
    public static void main(String[] args) {
    int PLAY = 0;
    Scanner sc = new Scanner(System.in);




    while(PLAY == 0){
        System.out.println("WELCOME TO THE LAND OF DRAGONS!");
        System.out.println("Dragons live in caves, guarding their hoards");
        System.out.println("Some are friendly, and will Share their treasure with a weary traveller");
        System.out.println("Others are hungry and will eat those who enter their caves!");
        System.out.println("The player will approach two caves: One with a friendly dragon...");
        System.out.println(" and one with a hungry dragon.");
        System.out.println("Instructions: To play, choose which cave to enter, (1 or 2)");
        System.out.println("Would you like to play?...(y/n): ");
        String START = sc.nextLine();
        int badCave = ((int) (Math.random() * 2)+ 1) ;


        if (START.equals("n")){
            PLAY++;
        }
        else{

            System.out.println("You are in a land full of dragons...");
            System.out.println("In front of you are two caves...");
            System.out.println("One with a friendly dragon that will share its treasure..");
            System.out.println("The other with a hungry dragon that will GOBBLE.YOU. UP...");

            System.out.println("Which cave will you enter?...(1 or 2):  ");
            int guess = sc.nextInt();

            if(guess == badCave){

                System.out.println("You approach the cave..");
                System.out.println("It's dark an spooky...");
                System.out.println("A large dragon jumps out in front of you!");
                System.out.println("It opens its jaws and...");
                System.out.println("Gobbles you down in one bite!");

                PLAY++;


            }
            else {
                System.out.println("You approach the cave..");
                System.out.println("It's dark an spooky...");
                System.out.println("A large dragon jumps out in front of you!");
                System.out.println("It opens its jaws and...");
                System.out.println("Welcomes you in to share in the treasure!");

                PLAY++;


            }
        }

    }

    System.out.println("Game Over...");

    }
}
